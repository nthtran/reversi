from Constants import *
from Board import *
from Logging import *

def strategyA(board, player):
    depth = 3
    setMaxIndent(depth)
    return minimax(board, player, depth)[0]
    
def strategyB(board, player):
    depth = 4
    setMaxIndent(depth)
    return minimax(board, player, depth, -100000, 100000)[0]

def strategyC(board, player):
    # intial depth is 4
    depth = 4
    totalScore = board.blackScore + board.whiteScore
    # if the number of tokens is bigger than 42, it means that there less
    # available moves to search through, increase the depth to 5
    if totalScore >= 42:
        depth = 5
    # if the number tokens is bigger than 55, there are even less available
    # moves, increae the depth to 7
    if totalScore >= 55:
        depth = 7
    # for logging    
    setMaxIndent(depth)


    moves = board.generateValidMoves(player)
    shallowMoves = []
    # as negascout relies on good move ordering we need to sort the moves before calling it.
    # for each move, check if it is one of the important moves (corner, X, C, A or B).
    # if it is then add it to the shallowMoves list with a value which indicates how good the move is.
    # the higher the value, the better the move:
    #   1000 for CORNERS - best move
    #   -1000 for X - worst move
    #   -500 for C - sort of bad move
    #   750 for A - ok move
    #   500 for B - ok move
    # the shallowMoves list will then be sorted in descending order based on the value.
    for move in moves:
        if move in CORNERS:
            shallowMoves.append((move, 1000))
        elif move in X_SQUARES:
            shallowMoves.append((move, -1000))
        elif move in C_SQUARES:
            shallowMoves.append((move, -500))
        elif move in A_SQUARES:
            shallowMoves.append((move, 750))
        elif move in B_SQUARES:
            shallowMoves.append((move, 500))
        else:
            shallowMoves.append((move, 0)) # if it's a normal move, set its value to 0
        
    # sort it in descending order
    sortedMoves = sorted(shallowMoves, key = lambda move : move[1], reverse=True)
    moves = [move[0] for move in sortedMoves]   
    # call negascout with sorted moves
    return negascout(board, player, depth, -100000, 100000, moves)[0]
    
# return a tuple with the first value being the best move (or None at max depth or game over) and
# the second being the heuristic value of that move
def minimax(board, player, depth, alpha = None, beta = None, moves = None):
    # if has reached the maximum depth, return the heuristic of the current board state
    if depth == 0:
        heuristic = board.getHeuristic(player)
        #writeToLog(getLog("Algorithm"), getIndent(depth) + str(heuristic) + "\n")
        return (None, heuristic)  
    # if the game is over, return the utility value of the terminal state
    if board.isGameOver() == True:
        heuristic = board.getUtilityValue(player)
        #writeToLog(getLog("Algorithm"), getIndent(depth) + str(heuristic) + " B: " + str(board.blackScore) + ", W: " + str(board.whiteScore) + "\n")
        return (None, heuristic) 

    # Logging stuff starts
    logDepthAndPlayer(depth, player)
    moveList = []
    
    bestMove = None
    # generate all valid moves for the current player
    if moves == None:
        moves = board.generateValidMoves(player)
    opponent = player*-1

    for move in moves:
        logMove(depth, move)   
            
        # create a copy of board
        board2 = Board(board)
        # do player's move on the new board2
        board2.placeToken(move, player)
        # if using alpha beta pruning
        if(alpha != None):
            # find the heuristic of that board state
            nextMove = minimax(board2, opponent, depth-1, -beta, -alpha)
        # if using minimax without alpha beta pruning
        else:
            # find the heuristic of that board state
            nextMove = minimax(board2, opponent, depth-1) # without using alpha beta pruning
        # to get the heuristic from the current player's perspective
        currentHeuristic = nextMove[1]*-1

        logAddToMoveList(moveList, move, currentHeuristic)
        
        # if using alpha beta pruning
        if(alpha != None):
            # if the move's heuristic > beta, we know that the other player will not choose this branch
            if (currentHeuristic >= beta):
                writeToLog(getLog("Algorithm"), getIndent(depth) + "Cut Off - heuristic: " + str(currentHeuristic) + ", beta: " + str(beta) + "\n")
                bestMove = (move, currentHeuristic)
                logBestMove(depth, bestMove, moveList)
                # Therefore we can prune the branch and not check the other states
                return bestMove
            # if this is the highest heuristic found so far
            if (bestMove == None or currentHeuristic > bestMove[1]):
                bestMove = (move, currentHeuristic)
                if (currentHeuristic > alpha):
                    alpha = currentHeuristic
        # if using minimax without alpha beta pruning
        else:
            # if this is the highest heuristic found so far
            if bestMove == None or currentHeuristic > bestMove[1]:
                # set min's move to be the best move
                bestMove = (move, currentHeuristic)

    logBestMove(depth, bestMove, moveList)    
    return bestMove

def negascout(board, player, depth, alpha, beta, moves = None):
    # if has reached the maximum depth, return the heuristic of the current board state
    if depth == 0:
        heuristic = board.getHeuristic(player)
        #writeToLog(getLog("Algorithm"), getIndent(depth) + str(heuristic) + "\n")
        return (None, heuristic)  
    # if the game is over, return the utility value of the terminal state
    if board.isGameOver() == True:
        heuristic = board.getUtilityValue(player)
        #writeToLog(getLog("Algorithm"), getIndent(depth) + str(heuristic) + " B: " + str(board.blackScore) + ", W: " + str(board.whiteScore) + "\n")
        return (None, heuristic)                     

    # Logging stuff starts
    logDepthAndPlayer(depth, player)
    moveList = []
    
    bestMove = None
    # generate all valid moves for the current player
    if moves == None:
        moves = board.generateValidMoves(player)
    opponent = player*-1
    b = beta
    i = 0
    
    # Now do a full search with given depth
    for move in moves:
        i += 1
        logMove(depth, move)   
            
        # create a copy of board
        board2 = Board(board)
        # do player's move on the new board2
        board2.placeToken(move, player)

        # find the heuristic of that board state
        nextMove = negascout(board2, opponent, depth-1, -b, -alpha)
        # to get the heuristic from the current player's perspective
        currentHeuristic = nextMove[1]*-1
        if currentHeuristic > alpha and currentHeuristic < beta and i > 1:
            nextMove = negascout(board2, opponent, depth - 1, -beta, -alpha)
            currentHeuristic = nextMove[1]*-1

        logAddToMoveList(moveList, move, currentHeuristic)

        if (bestMove == None or currentHeuristic > bestMove[1]):
            bestMove = (move, currentHeuristic)
            if (currentHeuristic > alpha):
                alpha = currentHeuristic
        
        if alpha >= beta:
            writeToLog(getLog("Algorithm"), getIndent(depth) + "Cut Off - alpha: " + str(alpha) + ", beta: " + str(beta) + "\n")
            logBestMove(depth, bestMove, moveList)
            return bestMove

        b = alpha + 1

    logBestMove(depth, bestMove, moveList)    
    return bestMove
