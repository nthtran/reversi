# Constants, should not change these variables

# x coordinates
XPOSITIONS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

# Tokens 
BLACK = -1
WHITE = 1
NONE = 0

# messages
INPUT_ERROR = 1
INVALID_MOVE = 2
HOW_TO = 3
PASSING = 4
GAME_OVER = 5
TURN = 6
SCORE = 7



# board size
SIZE = 8

# direction
LEFT = (-1, 0)
RIGHT = (1, 0)
UP = (0, 1)
DOWN = (0, -1)
TOP_LEFT = (-1, 1)
TOP_RIGHT = (1, 1)
BOTTOM_RIGHT = (1, -1)
BOTTOM_LEFT = (-1, -1)
DIRECTIONS = [LEFT, RIGHT, UP, DOWN, TOP_LEFT, TOP_RIGHT, BOTTOM_RIGHT, BOTTOM_LEFT]

CORNERS = [(0, 0), (0, 7), (7, 0), (7, 7)]
X_SQUARES = [(1, 1), (1, 6), (6, 1), (6, 6)]
C_SQUARES = [(1, 0), (6, 0), (1, 7), (6, 7), (0, 1), (0, 6), (7, 1), (7, 6)]
A_SQUARES = [(2, 0), (5, 0), (2, 7), (5, 7), (0, 2), (0, 5), (7, 2), (7, 5)]
B_SQUARES = [(3, 0), (4, 0), (3, 7), (4, 7), (0, 3), (0, 4), (7, 3), (7, 4)]

# settings
LOGGING = True
