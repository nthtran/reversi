from Constants import *
from datetime import datetime

logs = {}
maxIndent = 0
blackNumMoves = 0
whiteNumMoves = 0
blackTime = 0
whiteTime = 0

def newLog(fileName):
    if LOGGING == True:
        date = str(datetime.now())
        date = date.replace(':', '-')
        splitDate = date.split('.')
        date = splitDate[0]
        log = open("Logs/"+date+"_"+fileName+".txt", 'w')
        log.write("----Game at " + date + "_" + fileName + "----\n")
        logs[fileName] = log
        return log

    return None

def closeLog(log):
    if LOGGING == True:
        log.close()

def writeToLog(log, content):
    if LOGGING == True:
        log.write(content)

def getLog(logName):
    try:
        if LOGGING == True:
            return logs[logName]
    except KeyError:
        return None

def setMaxIndent(number):
    global maxIndent
    maxIndent = number

def getIndent(depth):
    string = ""
    if LOGGING == True:    
        for i in range(maxIndent - depth):
            string = string + "\t"
    return string
    
def logBestMove(depth, bestMove, moveList):
    algoLog = getLog("Algorithm")
    if LOGGING == True:
        writeToLog(algoLog, getIndent(depth) + str(moveList))
        move = bestMove[0]
        value = bestMove[1]
        if move == None:
            writeToLog(algoLog, "\n" + getIndent(depth) + "Best Move Chosen for depth " + str(depth) + " - " + "(" + "Pass" + ", " + str(value) + ")\n\n")
        else:
            writeToLog(algoLog, "\n" + getIndent(depth) + "Best Move Chosen for depth " + str(depth) + " - " + "(" + XPOSITIONS[move[0]] + str(move[1] + 1) + ", " + str(value) + ")\n\n")

def logMove(depth, move):
        algoLog = getLog("Algorithm")
        if move == None:
            writeToLog(algoLog, getIndent(depth) + "Pass" + "\n")
        else:
            writeToLog(algoLog, getIndent(depth) + XPOSITIONS[move[0]] + str(move[1] + 1) + "\n")
    
def logAddToMoveList(moveList, move, currentHeuristic):
    if LOGGING == True:
        if move == None:
            moveList.append(("Pass", currentHeuristic))
        else:
            moveList.append((XPOSITIONS[move[0]] + str(move[1] + 1), currentHeuristic))

def logDepthAndPlayer(depth, player):
    algoLog = getLog("Algorithm")
    writeToLog(algoLog, "\n" + getIndent(depth) + "Depth " + str(depth) + ":\n")
    
    if player == BLACK:
        writeToLog(algoLog, getIndent(depth) + "Black:\n")
    if player == WHITE:
        writeToLog(algoLog, getIndent(depth) + "White:\n")

def logTimeAndMoves(player, time):
    global blackTime
    global whiteTime
    global blackNumMoves
    global whiteNumMoves

    if player == WHITE:
        whiteTime += time
        whiteNumMoves += 1
    elif player == BLACK:
        blackTime += time
        blackNumMoves += 1

def logFinalInfo():
    global blackTime
    global whiteTime
    global blackNumMoves
    global whiteNumMoves

    if blackTime != 0 and blackNumMoves != 0:
        writeToLog(getLog("Moves"), "Black:\nAverage Time: " + str(blackTime/blackNumMoves) + " - Number of moves: " + str(blackNumMoves) + "\n")
        writeToLog(getLog("Algorithm"), "Black:\nAverage Time: " + str(blackTime/blackNumMoves) + "\nNumber of moves: " + str(blackNumMoves) + "\n")

    if whiteTime != 0 and whiteNumMoves != 0:
        writeToLog(getLog("Moves"), "White:\nAverage Time: " + str(whiteTime/whiteNumMoves) + " - Number of moves: " + str(whiteNumMoves) + "\n")
        writeToLog(getLog("Algorithm"), "White:\nAverage Time: " + str(whiteTime/whiteNumMoves) + "\nNumber of moves: " + str(whiteNumMoves) + "\n")
