from Constants import *
from Board import *
from Algorithms import *
from time import time
from Logging import *

class Game:
    # Constructor, taking in 2 possible parameters
    # No parameters starts a 2 player (human vs human) game
    # param1=strategy and param2=computerColour starts a 1 player (human vs AI) game
    # param1=strategy1 (black's strategy) and param2=strategy2 (white's strategy) starts a 0 player (AI vs AI) game
    def __init__(self, param1=None, param2=None):
	# set up a new board
        self.board = Board()
        # if it's a 2 player (Human vs Human game)
        if(param1 == None):
            self.players = 2
        # if it's a 1 player (Human vs AI) game
        elif(param2 == BLACK or param2 == WHITE):
            self.players = 1
            # set the strategy
            self.strategy = param1
            # set the AI's colour
            self.computerColour = param2
        # if it's a 0 player (AI vs AI) game
        elif(param2 in "ABC"):
            self.players = 0
            # set the black AI's strategy
            self.strategy1 = param1
            # set the white AI's strategy
            self.strategy2 = param2
        # black always plays first
        self.currentPlayer = BLACK
	
    # starts the game
    def startGame(self):
        # Create the log files
        log = newLog("Moves")
        algorithm = newLog("Algorithm")
        # Log some basic game information
        if(self.players == 2):
            writeToLog(log, "Human vs Human game" + "\n\n")
            writeToLog(algorithm, "Human vs Human game" + "\n\n")
        elif(self.players == 1):
            # find the human and AI's colour as a string for logging
            if(self.computerColour) == BLACK:
                ai = "Black"
                human = "White"
            else:
                ai = "White"
                human = "Black"
            writeToLog(log, "Human (" + human + ") vs AI (" + ai + ") game" + "\n")
            writeToLog(algorithm, "Human (" + human + ") vs AI (" + ai + ") game" + "\n")
            writeToLog(log, "AI using strategy " + self.strategy + "\n\n")
            writeToLog(algorithm, "AI using strategy " + self.strategy + "\n\n")
        elif(self.players == 0):
            writeToLog(log, "AI vs AI game" + "\n")
            writeToLog(algorithm, "AI vs AI game" + "\n")
            writeToLog(log, "Black AI using strategy " + self.strategy1 + "\n")
            writeToLog(algorithm, "Black AI using strategy " + self.strategy1 + "\n")
            writeToLog(log, "White AI using strategy " + self.strategy2 + "\n\n")
            writeToLog(algorithm, "White AI using strategy " + self.strategy2 + "\n\n")
            
	# print the how-to-play message
        self.printMessage(HOW_TO)
        board = self.board
	# display the board
        board.display()
        
	# This is the game loop, it keeps going until the game is over
        while True:
	    # print whose turn it is
            self.printMessage(TURN)
            # for logging purposes
            player = "Black" if (self.currentPlayer == -1) else "White"
        # if it is a 1 player game and it is the AI's turn, or if it is a 0 player game
            if((self.players == 1 and self.currentPlayer == self.computerColour) or self.players == 0):
            
            # if it is a 0 player game
                if(self.players == 0):
                # if black is the current player
                    if(self.currentPlayer == BLACK):
                    # the current strategy is black's strategy
                        self.strategy = self.strategy1
                # if white is the current player
                    elif(self.currentPlayer == WHITE):
                    # the current strategy is white's strategy
                        self.strategy = self.strategy2
                
                writeToLog(algorithm, player + " AI's turn " + "using strategy " + self.strategy + "\n")
                t1 = time()
                # find the current move
                if(self.strategy == "A"):
                    move = strategyA(board, self.currentPlayer)
                elif(self.strategy == "B"):
                    move = strategyB(board, self.currentPlayer)
                elif(self.strategy == "C"):
                    move = strategyC(board, self.currentPlayer)
                
                # do the current move
                currentMove = board.placeToken(move, self.currentPlayer)
                t2 = time()
                # printing logging stuff
                print "AI's move: " + XPOSITIONS[move[0]] + str(move[1] + 1)
                writeToLog(log, player + ": " + XPOSITIONS[move[0]] + str(move[1] + 1) + " " + str(t2 - t1) + "s\n")
                logTimeAndMoves(self.currentPlayer, t2 - t1)
            
            # If it is a 2 player game or a 1 player game and it is the human's turn
            else:
                # taking in user input
                move = raw_input("Your move: ")
                # read the move, checking for any input error
                currentMove = self.readMove(move)
            
                # Input error, try again
                if currentMove == None:
                    continue
            # place a token at the given position, try again if the move is not valid
                if board.placeToken(currentMove, self.currentPlayer) == False:
                    self.printMessage(INVALID_MOVE)
                    continue
                
                writeToLog(log, player + ": " + move + "\n")
            # display the board at the end of every turn
            board.display()
            # display score
            self.printMessage(SCORE)
	    # end the current turn, switch the player
            self.endTurn()
            
	    # check if the game is over (both players have no valid moves), if it is, break out of the loop
            if board.isGameOver():
                self.printMessage(GAME_OVER)
                break
            # else if it is not over yet, generate a list of valid moves for the current player (note: since
            # endTurn() is called earlier, the current player is now the opponent of the current player at the start
            # of this loop)
            else:
                validMoves = board.generateValidMoves(self.currentPlayer)
                # if no valid moves are available for the current player, it will be a pass for the current player,
                # therefore end turn, switch the player
                if validMoves[0] == None:
                    self.printMessage(PASSING)
                    self.endTurn()

        closeLog(log)
        closeLog(algorithm)
					
    # start a game from a file
    # the content of the file is a list of moves
    # by default the board is displayed after every turn (this can take quite long)
    # or you can choose to only display the end result, set showBoardEveryTurn to False
    def gameFromFile(self, fileName, showBoardEveryTurn=True):
        self.currentPlayer = WHITE # White goes first in this test
        # open file
        file = open(fileName, 'r')
	# from here it is similar to game2P(), the only difference is that the game stops
	# when an invalid move or input error is encountered
        self.printMessage(HOW_TO)
        board = self.board
    
        if showBoardEveryTurn == True:
            board.display()
        
        for move in file:
            self.printMessage(TURN)
            move = move.rstrip()
            if showBoardEveryTurn == True:
                print "Your move: " + move
            currentMove = self.readMove(move)
            
            if currentMove == None:
                break
                
            if board.placeToken(currentMove, self.currentPlayer) == False:
                self.printMessage(INVALID_MOVE)
                break

            if showBoardEveryTurn == True:
                board.display()
                self.printMessage(SCORE)
            
            self.endTurn()

            if board.isGameOver():
                self.printMessage(GAME_OVER)
                break
            else:
                validMoves = board.generateValidMoves(self.currentPlayer)
                if validMoves[0] == None:
                    self.printMessage(PASSING)
                    self.endTurn()

        if showBoardEveryTurn == False:
            board.display()
			
    # read the input from the player, return a tuple of the coordinates of the move if valid
    # else return none
    # the range of the coordinates are is >= 0 and < 7
    # but when reading the input, the user would type something like a3, 3g, B2, or 2B. these are all valid moves
    # the move is valid if:
    # 	- the integer is >=1 and <=8
    #   - the letter is from a to h, case-insensitive
    def readMove(self, move):
    # check if the length of the string is not 2, if it is not then it's invalids
        try:
            if len(move) != 2:
                self.printMessage(INPUT_ERROR)
                return
        except:
            self.printMessage(INPUT_ERROR)
            return

        firstChar = move[0]
        secondChar = move[1]
        # if the second character can be converted to a int,
        # then the second character must be the integer part, and the first character must be the letter part
        try:
            y = int(secondChar)
	    # check if the integer is in range
            if y > 8 or y < 1:
                self.printMessage(INPUT_ERROR)
                return
	    # check if the letter is one of the valid letters (from a to h)
            try:
                x = XPOSITIONS.index(firstChar.lower()) #return index
            except ValueError:
                self.printMessage(INPUT_ERROR)
                return
	# if the second character can't be converted to a int,
	# then the first character must be the integer part, and the second character must be the letter part
        except ValueError:
            try:
		# check if the integer is in range
                y = int(firstChar)
                if y > 8 or y < 1:
                    self.printMessage(INPUT_ERROR)
                    return
                # check if the letter is one of the valid letters (from a to h)
                try:
                    x = XPOSITIONS.index(secondChar.lower()) #return index
                except ValueError:
                    self.printMessage(INPUT_ERROR)
                    return
            except ValueError:
                self.printMessage(INPUT_ERROR)
                return
	# return a tuple of the x and y coordinates of the move
        return (x, y-1)
	
    # print a message
    def printMessage(self, message):
        if message == INPUT_ERROR:
            print "Wrong input format, please try again"
        elif message == PASSING:
            print "No legal move for",
            if self.currentPlayer == BLACK:
                print "Black",
            else:
                print "White",
            print "- Pass."
        elif message == HOW_TO:
            print "Enter moves such as a1, g5"
        elif message == INVALID_MOVE:
            print "Your move is not valid"
        elif message == GAME_OVER:
            log = getLog("Moves")
            algoLog = getLog("Algorithm")
            print "Game Over"
            writeToLog(log, "\nGame Over\n")
            writeToLog(algoLog, "Game Over\n")
            print "Black: " + str(self.board.blackScore) + " -",
            print "White: " + str(self.board.whiteScore)
            writeToLog(log, "Black: " + str(self.board.blackScore) + " - White: " + str(self.board.whiteScore) + "\n")
            writeToLog(algoLog, "Black: " + str(self.board.blackScore) + " - White: " + str(self.board.whiteScore) + "\n")
            if self.board.blackScore > self.board.whiteScore:
                print "Black wins"
                writeToLog(log, "Black wins\n")
                writeToLog(algoLog, "Black wins\n")
            elif self.board.whiteScore > self.board.blackScore:
                print "White wins"
                writeToLog(log, "White wins\n")
                writeToLog(algoLog, "White wins\n")
            else:
                print "Draw"
                writeToLog(log, "Draw\n")
                writeToLog(algoLog, "Draw\n")
            logFinalInfo()
                    
        elif message == TURN:
            if self.currentPlayer == BLACK:
                print "---Black's turn---"
            else:
                print "---White's turn---"

        elif message == SCORE:
            self.board.computeScore()
            print "Black: " + str(self.board.blackScore) + " -",
            print "White: " + str(self.board.whiteScore)
	
    # end the current turn, switching the player 
    def endTurn(self):
        self.currentPlayer = self.currentPlayer*-1
        
