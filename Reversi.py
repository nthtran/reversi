# Imports
from Constants import *
from Game import *

# main function, call this to run the game 
def main():
    # create a 0 player game (AI vs AI), Black uses strategy A, White uses strategy B
    #game = Game("C", "C")
    
    # create a 1 player game (Human vs AI)
    settings = chooseSettings()
    game = Game(settings[0], settings[1])
    
    # create a 2 player game (Human vs Human) 
    # game = Game()
    
    # start the game
    game.startGame()

def chooseSettings():
    players = ["human", "computer"]
    computer = None
    strategy = None
    
    while True:
        firstPlayer = raw_input("Please choose who goes first (human/computer):")
        if firstPlayer.lower() in players:
            computer = WHITE if firstPlayer.lower() == "human" else BLACK
            break
        else:
            continue

    while True:
        strategy = raw_input("Please choose which strategy you want the AI to play (A/B/C):")
        if not strategy.upper() in "ABC":
            continue
        else:
            strategy = strategy.upper()
            break
    return (strategy, computer)

def boardTest1():
    game = Game()
    game.gameFromFile('BoardTest1.txt', False)

def boardTest2():
    game = Game()
    game.gameFromFile('BoardTest2.txt', False)
    print game.board.blackScore,
    print game.board.whiteScore,
