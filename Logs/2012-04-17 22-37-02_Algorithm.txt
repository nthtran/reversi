----Game at 2012-04-17 22-37-02_Algorithm----
AI vs AI game
Black AI using strategy A
White AI using strategy B

Black AI's turn using strategy A

Depth 3:
Black:
c5

	Depth 2:
	White:
	c4

		Depth 1:
		Black:
		b3
		c3
		d3
		e3
		f3
		[('b3', -97), ('c3', 5), ('d3', 403), ('e3', 5), ('f3', -97)]
		Best Move Chosen for depth 1 - (d3, 403)

	c6

		Depth 1:
		Black:
		c7
		d6
		e3
		f4
		[('c7', -197), ('d6', 403), ('e3', -197), ('f4', -197)]
		Best Move Chosen for depth 1 - (d6, 403)

	e6

		Depth 1:
		Black:
		f3
		f4
		f5
		f6
		f7
		[('f3', 3), ('f4', 3), ('f5', 303), ('f6', 303), ('f7', 3)]
		Best Move Chosen for depth 1 - (f5, 303)

	[('c4', -403), ('c6', -403), ('e6', -303)]
	Best Move Chosen for depth 2 - (e6, -303)

d6

	Depth 2:
	White:
	c4

		Depth 1:
		Black:
		b3
		c3
		d3
		e3
		f3
		[('b3', 3), ('c3', 303), ('d3', 303), ('e3', 3), ('f3', 3)]
		Best Move Chosen for depth 1 - (c3, 303)

	c6

		Depth 1:
		Black:
		b6
		c5
		e3
		f4
		[('b6', -197), ('c5', 403), ('e3', -197), ('f4', -197)]
		Best Move Chosen for depth 1 - (c5, 403)

	e6

		Depth 1:
		Black:
		f3
		f4
		f5
		f6
		f7
		[('f3', -97), ('f4', 5), ('f5', 403), ('f6', 5), ('f7', -97)]
		Best Move Chosen for depth 1 - (f5, 403)

	[('c4', -303), ('c6', -403), ('e6', -403)]
	Best Move Chosen for depth 2 - (c4, -303)

e3

	Depth 2:
	White:
	d3

		Depth 1:
		Black:
		c2
		c3
		c4
		c5
		c6
		[('c2', -97), ('c3', 5), ('c4', 403), ('c5', 5), ('c6', -97)]
		Best Move Chosen for depth 1 - (c4, 403)

	f3

		Depth 1:
		Black:
		c5
		d6
		f4
		g3
		[('c5', -197), ('d6', -197), ('f4', 403), ('g3', -197)]
		Best Move Chosen for depth 1 - (f4, 403)

	f5

		Depth 1:
		Black:
		c6
		d6
		e6
		f6
		g6
		[('c6', 3), ('d6', 3), ('e6', 303), ('f6', 303), ('g6', 3)]
		Best Move Chosen for depth 1 - (e6, 303)

	[('d3', -403), ('f3', -403), ('f5', -303)]
	Best Move Chosen for depth 2 - (f5, -303)

f4

	Depth 2:
	White:
	d3

		Depth 1:
		Black:
		c2
		c3
		c4
		c5
		c6
		[('c2', 3), ('c3', 303), ('c4', 303), ('c5', 3), ('c6', 3)]
		Best Move Chosen for depth 1 - (c3, 303)

	f3

		Depth 1:
		Black:
		c5
		d6
		e3
		f2
		[('c5', -197), ('d6', -197), ('e3', 403), ('f2', -197)]
		Best Move Chosen for depth 1 - (e3, 403)

	f5

		Depth 1:
		Black:
		c6
		d6
		e6
		f6
		g6
		[('c6', -97), ('d6', 5), ('e6', 403), ('f6', 5), ('g6', -97)]
		Best Move Chosen for depth 1 - (e6, 403)

	[('d3', -303), ('f3', -403), ('f5', -403)]
	Best Move Chosen for depth 2 - (d3, -303)

[('c5', 303), ('d6', 303), ('e3', 303), ('f4', 303)]
Best Move Chosen for depth 3 - (c5, 303)

White AI's turn using strategy B

Depth 4:
White:
c4

	Depth 3:
	Black:
	b3

		Depth 2:
		White:
		b4

			Depth 1:
			Black:
			a3
			b5
			c3
			d3
			e3
			f3
			[('a3', -197), ('b5', -297), ('c3', 205), ('d3', 103), ('e3', -195), ('f3', -197)]
			Best Move Chosen for depth 1 - (c3, 205)

		b6

			Depth 1:
			Black:
			b5
			c3
			c6
			d3
			e3
			f3
			f4
			[('b5', 203), ('c3', 3), ('c6', 203), ('d3', 3), ('e3', -197), ('f3', -197), ('f4', 5)]
			Best Move Chosen for depth 1 - (b5, 203)

		c6

			Depth 1:
			Black:
			c3
			c7
			e3
			e6
			f4
			[('c3', -197), ('c7', -297), ('e3', -195), ('e6', 103), ('f4', -195)]
			Best Move Chosen for depth 1 - (e6, 103)

		d6

			Depth 1:
			Black:
			c3
			c7
			e3
			e6
			Cut Off - heuristic: 303, beta: 103
			[('c3', -97), ('c7', -297), ('e3', -195), ('e6', 303)]
			Best Move Chosen for depth 1 - (e6, 303)

		e6

			Depth 1:
			Black:
			d3
			e3
			f3
			f4
			f5
			f7
			[('d3', -97), ('e3', 3), ('f3', -197), ('f4', 5), ('f5', 3), ('f7', -97)]
			Best Move Chosen for depth 1 - (f4, 5)

		f6

			Depth 1:
			Black:
			d3
			e3
			f3
			f4
			Cut Off - heuristic: 5, beta: 5
			[('d3', -97), ('e3', 3), ('f3', -197), ('f4', 5)]
			Best Move Chosen for depth 1 - (f4, 5)

		[('b4', -205), ('b6', -203), ('c6', -103), ('d6', -303), ('e6', -5), ('f6', -5)]
		Best Move Chosen for depth 2 - (e6, -5)

	c3

		Depth 2:
		White:
		b4

			Depth 1:
			Black:
			a3
			a5
			b3
			d3
			e3
			f3
			[('a3', -297), ('a5', -297), ('b3', 103), ('d3', 303), ('e3', -195), ('f3', -297)]
			Best Move Chosen for depth 1 - (d3, 303)

		c6

			Depth 1:
			Black:
			c7
			d6
			Cut Off - heuristic: 305, beta: 303
			[('c7', -195), ('d6', 305)]
			Best Move Chosen for depth 1 - (d6, 305)

		e6

			Depth 1:
			Black:
			f3
			f4
			f5
			f6
			f7
			[('f3', 5), ('f4', 5), ('f5', 205), ('f6', 205), ('f7', 5)]
			Best Move Chosen for depth 1 - (f5, 205)

		[('b4', -303), ('c6', -305), ('e6', -205)]
		Best Move Chosen for depth 2 - (e6, -205)

	d3

		Depth 2:
		White:
		c2

			Depth 1:
			Black:
			b3
			b4
			c3
			d2
			e3
			f3
			f4
			[('b3', 3), ('b4', 3), ('c3', 3), ('d2', 703), ('e3', -97), ('f3', 3), ('f4', 3)]
			Best Move Chosen for depth 1 - (d2, 703)

		c6

			Depth 1:
			Black:
			b4
			b5
			b6
			d6
			e3
			f4
			f5
			[('b4', -99), ('b5', 205), ('b6', 101), ('d6', 301), ('e3', -99), ('f4', -99), ('f5', -99)]
			Best Move Chosen for depth 1 - (d6, 301)

		e2

			Depth 1:
			Black:
			b3
			b4
			c3
			d2
			Cut Off - heuristic: 703, beta: 301
			[('b3', 3), ('b4', 3), ('c3', -97), ('d2', 703)]
			Best Move Chosen for depth 1 - (d2, 703)

		e6

			Depth 1:
			Black:
			b4
			b5
			c3
			d6
			Cut Off - heuristic: 301, beta: 301
			[('b4', -99), ('b5', -99), ('c3', -99), ('d6', 301)]
			Best Move Chosen for depth 1 - (d6, 301)

		[('c2', -703), ('c6', -301), ('e2', -703), ('e6', -301)]
		Best Move Chosen for depth 2 - (c6, -301)

	e3

		Depth 2:
		White:
		c6

			Depth 1:
			Black:
			b3
			b4
			b5
			b6
			b7
			[('b3', 5), ('b4', 5), ('b5', 205), ('b6', 205), ('b7', 5)]
			Best Move Chosen for depth 1 - (b5, 205)

		Cut Off - heuristic: -205, beta: -301
		[('c6', -205)]
		Best Move Chosen for depth 2 - (c6, -205)

	f3

		Depth 2:
		White:
		b6

			Depth 1:
			Black:
			b3
			b4
			b5
			c3
			d3
			[('b3', -197), ('b4', 5), ('b5', 3), ('c3', 3), ('d3', -97)]
			Best Move Chosen for depth 1 - (b4, 5)

		Cut Off - heuristic: -5, beta: -301
		[('b6', -5)]
		Best Move Chosen for depth 2 - (b6, -5)

	[('b3', 5), ('c3', 205), ('d3', 301), ('e3', 205), ('f3', 5)]
	Best Move Chosen for depth 3 - (d3, 301)

c6

	Depth 3:
	Black:
	c7

		Depth 2:
		White:
		b5

			Depth 1:
			Black:
			a4
			a5
			b6
			c4
			d6
			e3
			f3
			f4
			[('a4', -197), ('a5', 7), ('b6', 103), ('c4', 103), ('d6', 3), ('e3', -197), ('f3', 5), ('f4', -197)]
			Best Move Chosen for depth 1 - (b6, 103)

		b7

			Depth 1:
			Black:
			a7
			d6
			Cut Off - heuristic: 203, beta: 103
			[('a7', -297), ('d6', 203)]
			Best Move Chosen for depth 1 - (d6, 203)

		c4

			Depth 1:
			Black:
			c3
			e3
			f3
			[('c3', -195), ('e3', -395), ('f3', -95)]
			Best Move Chosen for depth 1 - (f3, -95)

		d3

			Depth 1:
			Black:
			c3
			e3
			Cut Off - heuristic: 5, beta: -95
			[('c3', -97), ('e3', 5)]
			Best Move Chosen for depth 1 - (e3, 5)

		e6

			Depth 1:
			Black:
			d6
			f3
			Cut Off - heuristic: 5, beta: -95
			[('d6', -97), ('f3', 5)]
			Best Move Chosen for depth 1 - (f3, 5)

		f5

			Depth 1:
			Black:
			d6
			f3
			Cut Off - heuristic: 5, beta: -95
			[('d6', -97), ('f3', 5)]
			Best Move Chosen for depth 1 - (f3, 5)

		[('b5', -103), ('b7', -203), ('c4', 95), ('d3', -5), ('e6', -5), ('f5', -5)]
		Best Move Chosen for depth 2 - (c4, 95)

	d6

		Depth 2:
		White:
		c4

			Depth 1:
			Black:
			b3
			Cut Off - heuristic: 301, beta: 301
			[('b3', 301)]
			Best Move Chosen for depth 1 - (b3, 301)

		e6

			Depth 1:
			Black:
			b7
			c7
			d7
			Cut Off - heuristic: 301, beta: 301
			[('b7', 1), ('c7', -99), ('d7', 301)]
			Best Move Chosen for depth 1 - (d7, 301)

		[('c4', -301), ('e6', -301)]
		Best Move Chosen for depth 2 - (c4, -301)

	Cut Off - heuristic: 301, beta: 301
	[('c7', -95), ('d6', 301)]
	Best Move Chosen for depth 3 - (d6, 301)

e6

	Depth 3:
	Black:
	f3

		Depth 2:
		White:
		b5

			Depth 1:
			Black:
			b6
			c6
			Cut Off - heuristic: 301, beta: 301
			[('b6', 201), ('c6', 301)]
			Best Move Chosen for depth 1 - (c6, 301)

		c3

			Depth 1:
			Black:
			c4
			d3
			Cut Off - heuristic: 303, beta: 301
			[('c4', 203), ('d3', 303)]
			Best Move Chosen for depth 1 - (d3, 303)

		c4

			Depth 1:
			Black:
			b4
			c3
			c6
			d6
			e7
			f5
			f6
			[('b4', 3), ('c3', 3), ('c6', 203), ('d6', 203), ('e7', -195), ('f5', 205), ('f6', -297)]
			Best Move Chosen for depth 1 - (f5, 205)

		e3

			Depth 1:
			Black:
			d3
			f2
			f4
			Cut Off - heuristic: 303, beta: 205
			[('d3', -197), ('f2', -97), ('f4', 303)]
			Best Move Chosen for depth 1 - (f4, 303)

		[('b5', -301), ('c3', -303), ('c4', -205), ('e3', -303)]
		Best Move Chosen for depth 2 - (c4, -205)

	f4

		Depth 2:
		White:
		b5

			Depth 1:
			Black:
			b6
			c6
			Cut Off - heuristic: 301, beta: 301
			[('b6', 101), ('c6', 301)]
			Best Move Chosen for depth 1 - (c6, 301)

		c3

			Depth 1:
			Black:
			c4
			d3
			Cut Off - heuristic: 303, beta: 301
			[('c4', 203), ('d3', 303)]
			Best Move Chosen for depth 1 - (d3, 303)

		c4

			Depth 1:
			Black:
			b4
			c3
			c6
			d6
			Cut Off - heuristic: 305, beta: 301
			[('b4', -97), ('c3', -97), ('c6', 203), ('d6', 305)]
			Best Move Chosen for depth 1 - (d6, 305)

		e3

			Depth 1:
			Black:
			d2
			d6
			f2
			f3
			Cut Off - heuristic: 303, beta: 301
			[('d2', -97), ('d6', 203), ('f2', -97), ('f3', 303)]
			Best Move Chosen for depth 1 - (f3, 303)

		g3

			Depth 1:
			Black:
			e7
			f5
			f6
			f7
			g4
			[('e7', 5), ('f5', 103), ('f6', 103), ('f7', -197), ('g4', 203)]
			Best Move Chosen for depth 1 - (g4, 203)

		Cut Off - heuristic: -203, beta: -205
		[('b5', -301), ('c3', -303), ('c4', -305), ('e3', -303), ('g3', -203)]
		Best Move Chosen for depth 2 - (g3, -203)

	f5

		Depth 2:
		White:
		c4

			Depth 1:
			Black:
			c3
			d3
			d7
			e3
			e7
			[('c3', 3), ('d3', -299), ('d7', -399), ('e3', -197), ('e7', -399)]
			Best Move Chosen for depth 1 - (c3, 3)

		Cut Off - heuristic: -3, beta: -205
		[('c4', -3)]
		Best Move Chosen for depth 2 - (c4, -3)

	f6

		Depth 2:
		White:
		c4

			Depth 1:
			Black:
			c3
			d6
			e3
			e7
			[('c3', -97), ('d6', -399), ('e3', -297), ('e7', -399)]
			Best Move Chosen for depth 1 - (c3, -97)

		Cut Off - heuristic: 97, beta: -205
		[('c4', 97)]
		Best Move Chosen for depth 2 - (c4, 97)

	f7

		Depth 2:
		White:
		b5

			Depth 1:
			Black:
			b6
			c4
			Cut Off - heuristic: 301, beta: 301
			[('b6', 201), ('c4', 301)]
			Best Move Chosen for depth 1 - (c4, 301)

		c3

			Depth 1:
			Black:
			d3
			e3
			f3
			f5
			[('d3', 103), ('e3', 7), ('f3', -197), ('f5', -197)]
			Best Move Chosen for depth 1 - (d3, 103)

		Cut Off - heuristic: -103, beta: -205
		[('b5', -301), ('c3', -103)]
		Best Move Chosen for depth 2 - (c3, -103)

	[('f3', 205), ('f4', 203), ('f5', 3), ('f6', -97), ('f7', 103)]
	Best Move Chosen for depth 3 - (f3, 205)

[('c4', -301), ('c6', -301), ('e6', -205)]
Best Move Chosen for depth 4 - (e6, -205)

Black AI's turn using strategy A

Depth 3:
Black:
f3

	Depth 2:
	White:
	b5

		Depth 1:
		Black:
		b6
		c6
		d6
		e7
		f6
		[('b6', 201), ('c6', 301), ('d6', 201), ('e7', 3), ('f6', 1)]
		Best Move Chosen for depth 1 - (c6, 301)

	c3

		Depth 1:
		Black:
		c4
		d3
		e3
		e7
		f5
		f7
		[('c4', 203), ('d3', 303), ('e3', 203), ('e7', 5), ('f5', 3), ('f7', -197)]
		Best Move Chosen for depth 1 - (d3, 303)

	c4

		Depth 1:
		Black:
		b4
		c3
		c6
		d6
		e7
		f5
		f6
		[('b4', 3), ('c3', 3), ('c6', 203), ('d6', 203), ('e7', -195), ('f5', 205), ('f6', -297)]
		Best Move Chosen for depth 1 - (f5, 205)

	e3

		Depth 1:
		Black:
		d3
		f2
		f4
		f5
		f6
		f7
		[('d3', -197), ('f2', -97), ('f4', 303), ('f5', 3), ('f6', 103), ('f7', -197)]
		Best Move Chosen for depth 1 - (f4, 303)

	[('b5', -301), ('c3', -303), ('c4', -205), ('e3', -303)]
	Best Move Chosen for depth 2 - (c4, -205)

f4

	Depth 2:
	White:
	b5

		Depth 1:
		Black:
		b6
		c6
		d6
		e7
		f6
		[('b6', 101), ('c6', 301), ('d6', 103), ('e7', 3), ('f6', 1)]
		Best Move Chosen for depth 1 - (c6, 301)

	c3

		Depth 1:
		Black:
		c4
		d3
		d6
		e3
		e7
		f5
		f7
		[('c4', 203), ('d3', 303), ('d6', 3), ('e3', 203), ('e7', 5), ('f5', 103), ('f7', -197)]
		Best Move Chosen for depth 1 - (d3, 303)

	c4

		Depth 1:
		Black:
		b4
		c3
		c6
		d6
		e7
		f5
		f6
		[('b4', -97), ('c3', -97), ('c6', 203), ('d6', 305), ('e7', -195), ('f5', 305), ('f6', -297)]
		Best Move Chosen for depth 1 - (d6, 305)

	e3

		Depth 1:
		Black:
		d2
		d6
		f2
		f3
		f5
		f6
		f7
		[('d2', -97), ('d6', 203), ('f2', -97), ('f3', 303), ('f5', 203), ('f6', 103), ('f7', -197)]
		Best Move Chosen for depth 1 - (f3, 303)

	g3

		Depth 1:
		Black:
		e7
		f5
		f6
		f7
		g4
		[('e7', 5), ('f5', 103), ('f6', 103), ('f7', -197), ('g4', 203)]
		Best Move Chosen for depth 1 - (g4, 203)

	[('b5', -301), ('c3', -303), ('c4', -305), ('e3', -303), ('g3', -203)]
	Best Move Chosen for depth 2 - (g3, -203)

f5

	Depth 2:
	White:
	c4

		Depth 1:
		Black:
		c3
		d3
		d7
		e3
		e7
		[('c3', 3), ('d3', -299), ('d7', -399), ('e3', -197), ('e7', -399)]
		Best Move Chosen for depth 1 - (c3, 3)

	c6

		Depth 1:
		Black:
		c7
		d3
		d6
		d7
		e3
		e7
		f4
		[('c7', 3), ('d3', -197), ('d6', 803), ('d7', -97), ('e3', -197), ('e7', -97), ('f4', -197)]
		Best Move Chosen for depth 1 - (d6, 803)

	g4

		Depth 1:
		Black:
		e3
		e7
		f3
		f4
		f7
		g5
		[('e3', 3), ('e7', 3), ('f3', 3), ('f4', 103), ('f7', 3), ('g5', 603)]
		Best Move Chosen for depth 1 - (g5, 603)

	g6

		Depth 1:
		Black:
		e3
		e7
		f3
		f4
		f7
		g5
		[('e3', 3), ('e7', 3), ('f3', 3), ('f4', 3), ('f7', 3), ('g5', 603)]
		Best Move Chosen for depth 1 - (g5, 603)

	[('c4', -3), ('c6', -803), ('g4', -603), ('g6', -603)]
	Best Move Chosen for depth 2 - (c4, -3)

f6

	Depth 2:
	White:
	c4

		Depth 1:
		Black:
		c3
		d6
		e3
		e7
		[('c3', -97), ('d6', -399), ('e3', -297), ('e7', -399)]
		Best Move Chosen for depth 1 - (c3, -97)

	c6

		Depth 1:
		Black:
		c7
		d6
		e3
		e7
		f4
		[('c7', -97), ('d6', 305), ('e3', -297), ('e7', -97), ('f4', -297)]
		Best Move Chosen for depth 1 - (d6, 305)

	g6

		Depth 1:
		Black:
		e3
		e7
		f3
		f4
		f7
		g7
		[('e3', 3), ('e7', 3), ('f3', 3), ('f4', 3), ('f7', 3), ('g7', 503)]
		Best Move Chosen for depth 1 - (g7, 503)

	[('c4', 97), ('c6', -305), ('g6', -503)]
	Best Move Chosen for depth 2 - (c4, 97)

f7

	Depth 2:
	White:
	b5

		Depth 1:
		Black:
		b6
		c4
		d6
		e3
		f4
		f6
		[('b6', 201), ('c4', 301), ('d6', 201), ('e3', 3), ('f4', -199), ('f6', 1)]
		Best Move Chosen for depth 1 - (c4, 301)

	c3

		Depth 1:
		Black:
		d3
		e3
		f3
		f5
		[('d3', 103), ('e3', 7), ('f3', -197), ('f5', -197)]
		Best Move Chosen for depth 1 - (d3, 103)

	c4

		Depth 1:
		Black:
		b3
		c3
		d3
		e3
		f3
		f5
		[('b3', -97), ('c3', -97), ('d3', 203), ('e3', 7), ('f3', -197), ('f5', -197)]
		Best Move Chosen for depth 1 - (d3, 203)

	c6

		Depth 1:
		Black:
		c4
		c7
		d6
		e3
		f4
		f5
		f6
		[('c4', 303), ('c7', 3), ('d6', 203), ('e3', -195), ('f4', -297), ('f5', 305), ('f6', -97)]
		Best Move Chosen for depth 1 - (f5, 305)

	e7

		Depth 1:
		Black:
		d7
		f3
		f4
		f5
		f6
		[('d7', -197), ('f3', -197), ('f4', -197), ('f5', 3), ('f6', 103)]
		Best Move Chosen for depth 1 - (f6, 103)

	[('b5', -301), ('c3', -103), ('c4', -203), ('c6', -305), ('e7', -103)]
	Best Move Chosen for depth 2 - (c3, -103)

[('f3', 205), ('f4', 203), ('f5', 3), ('f6', -97), ('f7', 103)]
Best Move Chosen for depth 3 - (f3, 205)

