from Constants import *
    
class Board:
    # Board Constructor
    def __init__(self, board=None):
        # if we are creating a new Board object
        if(not board):
            # initialise a SIZExSIZE (8x8) board
            self.board = [[NONE for i in range(SIZE)]  for j in range(SIZE)]
            # place the starting tokens, 2 blacks and 2 whites
            self.board[3][3] = BLACK
            self.board[4][4] = BLACK
            self.board[3][4] = WHITE
            self.board[4][3] = WHITE
            # set the scores
            self.blackScore = 2
            self.whiteScore = 2
        # if we are copying a Board object
        else:
            # give the new Board object the same board list as the original Board object
            self.board = [[board.board[i][j] for j in range(SIZE)]  for i in range(SIZE)]
            # set the scores too
            self.blackScore = board.blackScore
            self.whiteScore = board.whiteScore
            
	
    # display the board, print 'x' for Black, 'o' for White and '-' for None
    # it also prints out the coordinates on each side of the board
    def display(self):
        board = self.board
        print " ",
        for index in range(len(XPOSITIONS)):
            print XPOSITIONS[index],
        print
        
        for y in range(SIZE):
            print SIZE - y,
            for x in range(SIZE):
                token = board[x][SIZE - 1 - y]
                if token == BLACK:
                    print 'b',
                elif token == WHITE:
                    print 'w',
                elif token == NONE:
                    print '-',
            print SIZE - y

        print " ",
        for index in range(len(XPOSITIONS)):
            print XPOSITIONS[index],
        print

    # computeScore
    def computeScore(self):
        self.blackScore = 0
        self.whiteScore = 0
	# loop through every possible tile on the board
        for x in range(SIZE):
            for y in range(SIZE):
                if self.getTokenColour((x, y)) == BLACK:
                    self.blackScore += 1
                elif self.getTokenColour((x, y)) == WHITE:
                    self.whiteScore += 1    
        
    # return True if the game is over
    # return False otherwise
    # this is the Terminal Test
    # counting the score in the process (should do the score counting in another function)
    def isGameOver(self):
	# loop through every possible tile on the board
        for x in range(SIZE):
            for y in range(SIZE):
		# if a tile is empty (NONE)
                if self.getTokenColour((x, y)) == NONE:
		# for each player (Black and White)
                    for player in [BLACK, WHITE]:
		# if it is possible for the player to place a token in this empty tile,
		# then return False (the game is not yet over)
                        if self.placeToken((x, y), player, True) == True:
                            return False

        return True
                        
    # generate the list of valid moves for a player,
    # return a list with the first value being None if no valid moves are available
    def generateValidMoves(self, player):
        validMoves = []
        for x in range(SIZE):
            for y in range(SIZE):
                if self.getTokenColour((x, y)) == NONE:
                    if self.placeToken((x, y), player, True) == True:
                        validMoves.append((x, y))

        if len(validMoves) == 0:
            validMoves.append(None)

        return validMoves

    def getTokenInDirection(self, move, direction):
        return (move[0] + direction[0], move[1] + direction[1])

    def isTokenValid(self, token):
        return (token[0] >= 0 and token[0] < SIZE and token[1] >= 0 and token[1] < SIZE)

    # return True if move is valid
    # return False otherwise
    # if testValid is False (by default), this method will place the token and flip other tokens
    # else if testvalid is True, this method will not place or flip tokens (used to only test if a move is valid, not to play the move)
    def placeToken(self, move, player, testValid=False):
        # if move is None (when passing), don't place any token
        if move == None:
            return True
        
        board = self.board
        opponent = player*-1

        successful = False
        
        if self.getTokenColour(move) != NONE or player != BLACK and player != WHITE:
            return successful
        
        for direction in DIRECTIONS:
            nextToken = self.getTokenInDirection(move, direction)
            if  (not self.isTokenValid(nextToken)) or self.getTokenColour(nextToken) != opponent:
                continue

            toFlipTokens = []
            
            while self.getTokenColour(nextToken) == opponent:
                if testValid == False:
                    toFlipTokens.append(nextToken)
                nextToken = self.getTokenInDirection(nextToken, direction)
                if not self.isTokenValid(nextToken):
                    break

            if self.isTokenValid(nextToken) and self.getTokenColour(nextToken) == player:
                successful = True
                if testValid == False:
                    self.setTokenColour(move, player)
                    for token in toFlipTokens:
                        self.setTokenColour(token, player)

        return successful
                
    def getTokenColour(self, token):
        return self.board[token[0]][token[1]]
        
    def setTokenColour(self, token, colour):
        self.board[token[0]][token[1]] = colour

    # returns the heuritic from player's perspective for a given board state
    # the heuristic is calculated using the number of tokens,
    # the number of corner tokens and the number of valid moves
    # each valid move is worth 100 tokens and corner tokens are worth 10 valid moves
    def getHeuristic(self, player):
        if player != WHITE and player != BLACK:
            raise Exception ("Invalid player colour, has to be either BLACK or WHITE")

        # if the game is over, use the utility function 
        if self.isGameOver():
            return self.getUtilityValue(player)
        
        opponent = player*-1
            
        # Number of tokens owned by max minus number of tokens owned by min
        tokenCount = 0;
        # Number of corner tokens owned by max minus number of corner tokens owned by min
        cornerTokens = 0;
        
        for i in range(SIZE):
            for j in range(SIZE):
                # current colour of the token
                tokenColour = self.board[i][j]
                # if the it is not a NONE token (either BLACK or WHITE)
                if tokenColour != NONE:
                    # for every corner token
                    if (i, j) in CORNERS:
                        # increase the corner token count
                        cornerTokens += 1*tokenColour*player
                    else:
                        # increase the token count
                        tokenCount += 1*tokenColour*player


        # get all of current player's valid moves
        validMovesMax = self.generateValidMoves(player)
        # get all of opponent's valid moves
        validMovesMin = self.generateValidMoves(opponent)
        # in case generateValidMoves returns None, as len(None) would result in runtime error
        if(validMovesMax[0] != None):
            validMovesMax = len(validMovesMax)
        else:
            validMovesMax = 0
        if(validMovesMin[0] != None):
            validMovesMin = len(validMovesMin)
        else:
            validMovesMin = 0
        # number of valid moves for max minus number of valid moves for min
        validMoves = validMovesMax - validMovesMin
        heuristic = validMoves * 100 + cornerTokens * 1000 + tokenCount
        return heuristic

    # The utility function, return 10000 as a win, -10000 as a loss and 0 as a draw for the given player
    # It does not check if the game is over (isGameOver()), therefore only call it after checking that
    def getUtilityValue(self, player):
        self.computeScore()
        if self.whiteScore > self.blackScore:
            return 10000*player
        elif self.blackScore > self.whiteScore:
            return -10000*player
        else:
            return 0        
